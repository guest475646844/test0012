Cache repository for [test0001 repository](https://gitlab.torproject.org/guest475646844/test0001) to avoid rebuilding same packages.

Git tag - [tbb-12.0.2-build1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/tree/tbb-12.0.2-build1) (release variant).

Build command: `$(rbm) build application-services --target no_containers --target release --target torbrowser-android-aarch64`:

  1. clang-14.0.4 ([773 MB](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232352/artifacts/raw/tor-browser-build/out/clang/clang-14.0.4-4b29dd.tar.gz), [sha256](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232352/artifacts/raw/t0001/out.sha256), [logs](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232222)).

  2. rust-1.60.0 ([661 MB](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232471/artifacts/browse/tor-browser-build/out/rust) (directory), [sha256](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232471/artifacts/raw/t0001/out_rust.sha256), [logs](https://gitlab.torproject.org/guest475646844/test0012/-/jobs/232222)).






